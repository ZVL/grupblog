<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<link href="<?=ROOT?>styles.css" rel="stylesheet" type="text/css">
	<title><?=$title?></title>
</head>
<body>
	<div id="wrapper">
		<header>
			<div class="header line">
				<div class="wrapper">
					<div class="logo"></div>
					<div class="slogan">
						<div class="title">Рога и копыта</div>
						<div class="subtitle">Всем по рогам, всем по копытам.</div>
					</div>
					<div class="phone">8 800 800 80 80</div>
				</div>
			</div>		
			<div class="menu line">
				<div class="wrapper">
					<nav>
						<div class="show_menu">Меню</div>
						<ul>
                           
						</ul>					
					</nav>
				</div>
			</div>		
		</header>
		<section>
			<div class="content line">
				<div class="wrapper">
					<?=$content?>
				</div>
			</div>
		</section>
		<footer>
			<div class="footer line">
				<div class="wrapper">
					<span class="copy">&copy; ООО Рога и Копыта, Москва 2014, все права защищены!</span>
				</div>
			</div>
		</footer>
	</div>	
</body>
</html>