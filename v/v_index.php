<div class="comments">
    <? foreach($comments as $one): ?>
        <div class="item">
            <span><?=$one['dt']?></span>
            <strong><?=$one['name']?></strong>
            <div><?=$one['text']?></div>
            <a href="<?=ROOT?>messages/one/<?=$one['id_message']?>">Перейти</a>
            <a href="<?=ROOT?>messages/del/<?=$one['id_message']?>">Удалить</a>
        </div>
        <hr>
    <? endforeach; ?>
</div>
<a href="<?=ROOT?>messages/add">Написать</a>