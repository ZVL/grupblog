<?php

namespace C;

use M\System;

abstract class Admin extends Base{
    protected $title;
    protected $content;
    protected $params;
    protected $user;
    protected $needAuth;
    
    public function __construct(){
        parent::__construct();
        $this->title = 'Наш сайт - ';
        $this->content = '';
        // $this->user = (new M_auth())->getUser();
        $this->needAuth = true;
    }
    
    public function load($params){
        parent::load($params);
        
        if($this->needAuth && $this->user == null){
            exit();
        }
    }

    public function render(){
        $html = System::template('admin/v_main.php', [
            'title' => $this->title,
            'content' => $this->content
         ]);
         
        return $html;
    } 
}