<?php

namespace C;

use M\Messages as Model;
use M\System;

class Messages extends Client{
    
    public function action_index(){
        $mMessages = Model::instance();
        $comments = $mMessages->all();
        
        $this->title .= 'главная';      
        $this->content = System::template('v_index.php', [
            'comments' => $comments
         ]);
    }    
    
    public function action_one(){
        $mMessages = Model::instance();
        $one = $mMessages->one($this->params[2]);
        
        if($one == null){
            $this->show404();
            return;
        }
        
        $this->title .= 'просмотр сообщения';
        
        $this->content = System::template('v_one.php', [
            'message' => $one
        ]);
    }
    
    public function action_add(){
        $mMessages = Model::instance();
        
        if(count($_POST) > 0){
            $name = trim($_POST['name']);
            $text = trim($_POST['text']);

            if($name != '' && $text != ''){   
                $mMessages->add($name, $text);
                // в сессию положить $_SESSION['msg'] = 'jdfhedfiuw'
                header("Location: " . ROOT . 'messages');
                exit();
            }
        }
        else{
            $name = '';
            $text = '';
        }
    
        $this->title = 'добавление сообщения';
        
        $this->content = System::template('v_add.php', [
            'name' => $name,
            'text' => $text
        ]);
    }
    
    public function action_edit(){
        var_dump($this->params);
        
        $id = (int)$this->params[2];
    }
    
    public function action_del(){
        $mMessages = Model::instance();
        $mMessages->delete($this->params[2]);
        header("Location: " . ROOT . 'messages');
        exit();
    }    
}
