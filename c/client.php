<?php

namespace C;

use M\System;

abstract class Client extends Base{
    protected $title;
    protected $content;
    protected $params;
    
    public function __construct(){
        $this->title = 'Наш сайт - ';
        $this->content = '';
    }

    public function show404(){
        // header 404
        $this->title .= 'ошибка 404'; 
        $this->content = System::template('v_404.php');
    }
    
    public function render(){
        $html = System::template('v_main.php', [
            'title' => $this->title,
            'content' => $this->content
         ]);
         
        return $html;
    } 
}
