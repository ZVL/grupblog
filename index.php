<?php

    function my_class_loader($classname){
        $classname = strtolower($classname);
        $classname = str_replace('\\', '/', $classname);
        include_once($classname . '.php');
    }

    spl_autoload_register('my_class_loader');
    
    define('ROOT', '/');

    $params = explode('/', $_GET['q']);
    $cnt = count($params);

    if($params[$cnt - 1] === ''){
        unset($params[$cnt - 1]);
    }
    
    $param0 = isset($params[0]) ? $params[0] : 'messages';
    $controllers = ['messages', 'pages'];

    if(in_array($param0, $controllers)){
        $cname = 'C\\' . ucfirst($param0);
        $action = isset($params[1]) ? 
                    'action_' . $params[1] : 
                    'action_index';
    }
    else{
        $cname = 'C\\Pages';
        $action = 'show404';
    }
    
    
    $controller = new $cname();
    $controller->load($params);
    $controller->$action();
    $html = $controller->render();
    
    echo $html;