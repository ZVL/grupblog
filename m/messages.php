<?php
 
namespace M;    
        
use Core\Sql;        
        
class Messages{ 
    use \Core\Traits\Singleton;

    protected $db;
    
    protected function __construct(){
        $this->db = Sql::instance();
    }
    
    public function add($name, $text){
        /* а тут возможна валидация и любые другие действия */
        
        return $this->db->insert('messages', [
            'name' => $name,
            'text' => $text
        ]);
    }
    
    public function all(){
        return $this->db->select("SELECT * FROM messages ORDER BY dt DESC");
    }
    
    public function one($id_message){
        $res = $this->db->select("SELECT * FROM messages WHERE id_message=:id",
                                   ['id' => $id_message]
                                   );

        return $res[0] ?? null;
    }
    
    public function delete($id_message){
        return $this->db->delete('messages', "id_message=:id", ['id' => $id_message]);
    }
}