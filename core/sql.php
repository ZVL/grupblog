<?php
 
namespace Core; 
 
use PDO;

class Sql{
    use \Core\Traits\Singleton;
    
    protected $db;
    
    public function __construct(){
        $this->db = new PDO('mysql:host=localhost;dbname=php1project', 'root', '', [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]);
        
        $this->db->exec("SET NAMES UTF8"); 
    }

    public function select($sql, $params = []){
        $query = $this->db->prepare($sql);
        $query->execute($params);
        $this->check_query($query);
        return $query->fetchAll();
    }
    
    /*
        [
            'name' => $name,
            'text' => $text
        ]
    */
    
    public function insert($table, $obj){
        $keys = [];
        $masks = [];
        
        foreach($obj as $k => $v){
            $keys[] = $k;
            $masks[] = ':' . $k;
        }
        
/*		echo"$k___"; print_r($keys);echo"<hr>";
		echo"$mask___"; print_r($masks);echo"<hr>";
		die;
*/		
        $fields = implode(', ', $keys);
        $values = implode(', ', $masks);
        
        $sql = "INSERT INTO $table ($fields) VALUES ($values)";

        $query = $this->db->prepare($sql);
        $query->execute($obj);
        $this->check_query($query);
        
        return $this->db->lastInsertId();
    }
    
    public function update($table, $obj, $where, $params = []){
        /* a=:a, b=:b */
        /* UPDATE $table SET $pairs WHERE $where */
		// $pairs='key[obj]'=:key
		$pair=[];
	    foreach($obj as $k => $v){
            $pair[]= "$k=:$k";
            }
        $pairs=implode(',',$pair);
		
		
		$merge=array_merge($obj,$params);
		
        $sql="UPDATE $table SET $pairs WHERE $where";
	        $query = $this->db->prepare($sql);
		    $query->execute($merge);
            $this->check_query($query);

	
	}
    
    public function delete($table, $where, $params = []){
        $sql = "DELETE FROM $table WHERE $where";
        $query = $this->db->prepare($sql);
        $query->execute($params);
        $this->check_query($query);
        /* rowCount - количество затронутых строк */
        return true;
    }
    
    protected function check_query($query){
        if($query->errorCode() != PDO::ERR_NONE){
            $info = $query->errorInfo();
            echo implode('<br>', $info);
            exit();
        }
    }
}